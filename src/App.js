import React, { Component } from 'react';
import { Route} from 'react-router-dom';
import cookie from 'react-cookies';
import swal from 'sweetalert';
import firebase from 'firebase';
import {DB_CONFIG} from './Config';

import Login from './components/Login';
import Footer from './components/Footer';

import Navbar from './components/Navbar';
import SidebarMhs from './components/SidebarMhs';
import Home from './components/Home';
import DataPeserta from './components/DataPeserta';
import Qrcode from './components/Qrcode';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role : 0
    };
  }

  componentWillMount(){
    firebase.initializeApp(DB_CONFIG);
    if (cookie.load('access') !== undefined && cookie.load('role') === '1') {
      this.setState({role : 1})
      
    }
    if (cookie.load('access') === undefined && cookie.load('role') === undefined && window.location.pathname !== '/') {this.setState({role : 0})}
    if(cookie.load('role') === undefined && window.location.pathname !== '/') {window.location = "/"}
  }

  render() {
    const {role} = this.state
    let content = null
    if (role === 0) {
      content =
        <div id="wrapper" className="wrapper">
          <Route exact path="/" component={Login}/>
        </div>
    }else if (role === 1){
      content =
        <div id="wrapper">
          <Navbar/>
          <SidebarMhs/>
          <Route exact path="/" component={Home}/>
          <Route exact path="/dashboard" component={Home}/>
          <Route exact path="/data" component={DataPeserta}/>
          <Route exact path="/qrcode" component={Qrcode}/>
          
          <div className="clearfix"></div>
		      <footer>
            <Footer/>
          </footer>
        </div>
    }
    return (
      <div>
        {content}
      </div>
    );
  }
}

export default App;
