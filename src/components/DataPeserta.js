import React, { Component } from 'react';
import firebase from 'firebase';
import cookie from 'react-cookies';
import swal from 'sweetalert';

class DataPeserta extends Component{
    constructor(props){
        super(props);
        this.state ={
            nama    :'',
            nim     :'',
            jurusan :'',
            kode_prodi:'',
            foto    :'',
            kke     :'',
            email   :'',
            no_hp   :'',
            pass    :'',
            passLama    : '',
            passBaru    : '',
            passUBaru   : ''
        }
    }
    componentWillMount(){
        
    }
    
    render() {
        return(
            <div className="main">
                <div className="main-content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-16">
                                <div className="panel">
                                    <div className="panel-heading">
                                        <h1 className="panel-title"><b>Form Pendaaftaran</b></h1><br/>
                                        <div className="table-responsive">
                                            <table className="table table-hover">
                                            <div class="container">
                                                <h2>Tabel Data Peserta</h2>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
                                                <tr>
                                                <th width="7%" scope="col">No</th>
                                                <th width="17%" scope="col">Nama</th>
                                                <th width="17%" scope="col">Nomor</th>
                                                <th width="12%" scope="col">Email</th>
                                                <th width="12%" scope="col">Tanggal</th>
                                                <th width="12%" scope="col">Status</th>
                                               
                                                <th width="19%" scope="col">Aksi</th>
                                                </tr>
                                                <tr>
                                                <td>1</td>
                                                <td>Bayu</td>
                                                <td>0853388172626</td>
                                                <td>Bayu@gmail.com</td>
                                                <td>1029</td>
                                                <td>Hadir</td>
                                               
                                                <td> 
                                                <a href="3"><label class="btn btn-danger" >Delete</label></a></td>
                                                </tr>
                                                
                                                </table>
                                                </div>

                                            </table>
                                        </div>
                                        

                                        
                                    </div>
                                </div>
                            </div>
                            
                            

                           

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default DataPeserta