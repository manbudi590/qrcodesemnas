import React, { Component } from 'react';
import cookie from 'react-cookies';
import logo from '../assets/img/logo-dark.png';
import logoo from '../assets/img/logo-mhs.png';
import off from '../assets/img/offline.png';
import on from '../assets/img/online.png';
import { Offline, Online } from "react-detect-offline";

class Navbar extends Component {
    handleLogout= () =>{
        cookie.remove('user_id')
        cookie.remove('access')
        window.location = "/";
    }
    render() {
        return(
            <nav className="navbar navbar-default navbar-fixed-top">
                <div className="brand">
                    <a ><img src={logoo} alt="Klorofil Logo" className="img-responsive logo"/></a>
                </div>
                <div className="container-fluid">
                    <div className="navbar-btn">
                        <button type="button" className="btn-toggle-fullwidth"><i className="lnr lnr-arrow-left-circle"></i></button>
                    </div>
                    <div id="navbar-menu">
                        <ul className="nav navbar-nav navbar-right">
                            <a className="navbar-btn">
                                <p>Infornation 2019</p>
                            </a>
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}
export default Navbar