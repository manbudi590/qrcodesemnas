import React, { Component } from 'react'
import '../assets/css/main.css'
import logo from '../assets/img/png logo unram 100x100.png'
import skke from '../assets/img/logo-dark.png'
import axios from 'axios';
import cookie from 'react-cookies';
import swal from 'sweetalert';
import firebase from 'firebase';
import { PropagateLoader } from 'react-spinners';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            cekDB: '',
            loading: false,
            nimForget: '',
            emailForget: ''
        }
        this.handleChangeNIM = this.handleChangeNIM.bind(this)
        this.handleChangePassword = this.handleChangePassword.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.kirimEmailPass = this.kirimEmailPass.bind(this)
    }

    componentWillMount() {
        axios.get("https://api.github.com/users/KrunalLathiya")
            .then(response => {
                this.setState({
                    company: response.data.company,
                    blog: response.data.blog,
                    avatar: response.data.avatar_url,
                    loading: false
                });
            })
            .catch(error => {
                console.log(error);
            });
    }
    handleChangeNIM(event) { this.setState({ nim: event.target.value }) }
    handleChangePassword(event) { this.setState({ password: event.target.value }) }
   
    kirimEmailPass() {
        const databaseRef = firebase.database().ref();
        const todosRef = databaseRef.child("user").child(this.state.nimForget);
        swal({
            title: "Pilih OK untuk lanjut mengirim password",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    todosRef.on('value', snap => {
                        if (snap.exists() !== false) {
                            const from_email = 'chaerussulton@gmail.com';
                            const from_name = 'Admin e-SKKE';
                            const to_email = snap.val().email;
                            const to_name = snap.val().nama;
                            const subject = 'Password akun e-SKKE';
                            const content = 'Password akun e-SKKE Anda adalah ' + snap.val().password;
                            const send = 'https://api.lrsoft.id/mail/v1/send?' +
                                'to_email=' + to_email + '&' +
                                'to_name=' + to_name + '&' +
                                'from_email=' + from_email + '&' +
                                'from_name=' + from_name + '&' +
                                'content=' + content + '&' +
                                'subject=' + subject;
                            axios.post(send)
                                .then(function (response) {
                                    console.log(response.data.code);
                                    if (response.data.code === 'success') {
                                        swal("Success!", "Password telah dikirim ke email " + snap.val().email, "success");
                                    } else {
                                        swal("Oops!", "Pengiriman password ke email " + snap.val().email + " gagal!", "error");
                                    }
                                    setTimeout(() => {
                                        window.location = "/";
                                    }, 5000);
                                });
                        } else {
                            swal("Oops!", "NIM belum terdaftar!", "error");
                        }
                    });
                } else { }
            });
    }
    handleSubmit(event) {
        const self = this
        self.setState({ loading: true })
        event.preventDefault()
        const databaseRef = firebase.database().ref();
        if (this.state.nim !== undefined && this.state.password !== '') {
                const todosRef = databaseRef.child("user").child(this.state.nim);
                todosRef.on('value', snap => {
                    if (snap.exists() !== false) {
                        if (snap.val().password === this.state.password) {
                            cookie.save('user_id', this.state.nim, {
                                path: '/'
                            })
                            cookie.save('access', this.state.nim, {
                                path: '/'
                            })
                            cookie.save('role', 1, {
                                path: '/'
                            })
                            setTimeout(() => {
                                window.location = "/dashboard";
                            }, 0);
                        } else if (snap.val().password !== this.state.password) {
                            this.setState({
                                loading: false
                            })
                            swal("Oops!", "Password salah!", "error");
                        }
                    }else{
                        this.setState({
                            loading: false
                        })
                        swal("Oops!", "Username salah!", "error");
                    } 
                });
            
        } else {
            this.setState({
                loading: false
            })
            swal("Oops!", "Username atau Password salah!", "error");
        }
    }

    render() {
        return (
            <div>
                <div id="wrapper">
                    <div className='padLogin' />
                    <div className="vertical-align-wrap">
                        <div className="vertical-align-middle">
                            <div className="auth-box ">
                                <div className="left">
                                    <div className="content">
                                        <div className="header">
                                            <div className="logo text-center"><img src={logo} alt="Klorofil Logo" /></div>
                                        </div>
                                        <div className="form-auth-small">
                                            <form id="loginform" onSubmit={this.handleSubmit}>
                                                <div className="input-group">
                                                    <span className="input-group-addon"><i className="fa fa-user"></i></span>
                                                    <input type="text" className="form-control" name="username" placeholder="Username" onChange={this.handleChangeNIM} />
                                                </div><br />
                                                <div className="input-group">
                                                    <span className="input-group-addon"><i className="fa fa-key"></i></span>
                                                    <input type="password" className="form-control" name="password" id="signin-password" placeholder="Password" onChange={this.handleChangePassword} />
                                                </div><br />
                                                <div className='sweet-loading' style={{ 'marginLeft': '46%', 'paddingBottom': 10 }}>
                                                    <PropagateLoader
                                                        color={'#9013FE'}
                                                        loading={this.state.loading}
                                                    />
                                                </div>
                                                <div className="form-group text-center no-gutters mb-4">
                                                    <button className="btn btn-primary btn-lg btn-block" type="submit">{this.state.loading ? "Loading..." : "Login"}</button>
                                                    <div className="bottom">
                                                        <span className="helper-text"><i className="fa fa-lock"></i> <a href="#" data-toggle="modal" data-target="#forget"> Forgot Password?</a></span>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div className="right">
                                    <div className="overlay"></div>
                                    <div className="content text">
                                        <h1 className="heading">Website Pendaftaran Seminar Nasional Infornation 2019</h1>
                                        <p>Fakultas Teknik Universitas Mataram</p>
                                    </div>
                                </div>
                                <div className="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="forget" className="modal fade" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="vertical-align-wrap">
                        <div className="vertical-align-middle">
                            <div className="auth-box lockscreen clearfix">
                                <div className="content">
                                    <h2 className="text-center"><b>Forgot Password</b></h2>
                                    <div className="logo text-center"><img src={logo} alt="UNRAM Logo" /></div>
                                    <div className="user text-center">
                                        <img src={skke} alt="E-SKKE" />
                                    </div>
                                    <div className="input-group">
                                        <input type="text" className="form-control" placeholder="Enter your NIM ..." onChange={this.nimForgetPass} />
                                        <span className="input-group-btn"><button type="submit" className="btn btn-primary" onClick={this.kirimEmailPass}><i className="fa fa-arrow-right"></i></button></span>
                                    </div>
                                    <h5 className="name" style={{ 'color': 'red' }}>*Password akan dikirim melalui Email yang terdaftar pada akun E-SKKE.</h5>
                                </div>
                                <div className="logo text-center">
                                    <button type="button" className="btn btn-primary" data-dismiss="modal">Kembali</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Login