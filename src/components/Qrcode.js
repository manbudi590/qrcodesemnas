import QRCode from 'qrcode.react';
import React, { Component } from 'react';
import firebase from 'firebase';
import cookie from 'react-cookies';
import swal from 'sweetalert';
import axios from 'axios';

class Qrcode extends Component{
    constructor(props){
        super(props);
        
        this.state ={
            nama    :'',
            nomor    :'',
            email :'',
            tgl:'',
            status    :'',
            
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.downloadQR = this.downloadQR.bind(this)
        this.kirimEmailPass = this.kirimEmailPass.bind(this)
    }
    componentWillMount(){
   
        // downloadQR = () => {
        //     const canvas = document.getElementById("123456");
        //     const pngUrl = canvas
        //       .toDataURL("image/png")
        //       .replace("image/png", "image/octet-stream");
        //     let downloadLink = document.createElement("a");
        //     downloadLink.href = pngUrl;
        //     downloadLink.download = "123456.png";
        //     document.body.appendChild(downloadLink);
        //     downloadLink.click();
        //     document.body.removeChild(downloadLink);
        //   };

        // const self = this
        // const getNim = cookie.load('access')
        // const databaseRef = firebase.database().ref();
        // const todosRef = databaseRef.child("user").child(getNim);
        // todosRef.on('value', snap => {
        //     if (snap.exists() !== false){
        //         self.setState({
        //             nama    : snap.val().nama,
        //             kode_prodi: snap.val().jurusan,
        //             nim     : cookie.load('user_id'),
        //             foto    : snap.val().foto,
        //             kke     : snap.val().kke,
        //             email   : snap.val().email,
        //             no_hp   : snap.val().no_hp,
        //             pass    : snap.val().password
        //         })
        //     }else{
        //         console.log('Data kosong!')
        //     }
        // })
       
    }

    kirimEmailPass() {
        const databaseRef = firebase.database().ref();
        
        swal({
            title: "Pilih OK untuk lanjut QRcode",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    const from_email = 'manbudi590@gmail.com';
                    const from_name = 'Admin Seminar Nasional';
                    const to_email = cookie.load('email');
                    const to_name = cookie.load('nama');
                    const subject = 'QRcode Seminar';
                    const content = 'Ne QRcodde m kanak' + cookie.load('email');
                    const send = 'https://api.lrsoft.id/mail/v1/send?to_email=' + to_email +'&to_name=' + to_name +'&from_email=' + from_email +'&from_name=' + from_name +'&content=' + content +'&subject=' + subject
                    axios.post(send)
                        .then(function (response) {
                            console.log(response.data.code);
                            if (response.data.code === 'success') {
                                swal("Success!", "QRcode telah dikirim ke email " + cookie.load('email'), "success");
                            } else {
                                swal("Oops!", "Pengiriman password ke email " + cookie.load('email') + " gagal!", "error");
                            }
                            // setTimeout(() => {
                            //     window.location = "/";
                            // }, 5000);
                        });
                    }       
            });
    }

    handleSubmit(event) {
        const self = this
        self.setState({ loading: true })
        event.preventDefault()
        const databaseRef = firebase.database().ref();
            const todosRef = databaseRef.child("peserta").child(this.state.nomor);
                setTimeout(() => {
                    todosRef.set({
                        Nama: this.state.nama,
                        Nomor: this.state.nomor,
                        Email: this.state.email,
                        Tanggal: this.state.tgl,
                        status: this.state.status,
                    });
                }, 0);
                this.setState({
                    loading: false
                })
                swal("Oops!", "Berhasil", "Succes");
            
        
    }

   
    downloadQR () {
        const canvas = document.getElementById("123456");
        const pngUrl = canvas
          .toDataURL("image/png")
          .replace("image/png", "image/octet-stream");
        let downloadLink = document.createElement("a");
        downloadLink.href = pngUrl;
        downloadLink.download = cookie.load('email')+".png";
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    };
    

    
    
    render() {
        return(
            <div className="main">
                <div className="main-content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-15">
                                <div className="panel">
                                    <div className="panel-heading">
                                        <h1 className="panel-title"><b>Form Pendaaftaran</b></h1><br/>
                                        <div className="table-responsive">
                                            <table className="table table-hover">
                                            <div class="container">

                                            <form class="form-signin">
                                            <center>
                                                <h2 class="form-signin-heading">QR Code Generator</h2>
                                                
                                                <div>
                                                <QRCode   
                                                            id="123456"
                                                            value={cookie.load('nama')+'/'+cookie.load('email')+'/'+cookie.load('nomor')}
                                                            size={290}
                                                            level={"H"}
                                                            includeMargin={true}
                                                    />
                                                </div>
                                                <div className="row">
                                                    <a onClick={this.kirimEmailPass}> Download QR </a>
                                                    <span className="input-group-btn"><button type="submit" className="btn btn-primary" onClick={this.kirimEmailPass}><i className="fa fa-arrow-right"></i></button></span>
                                                    
                                        </div>
                                        </center>
                                            </form>

                                            </div>

                                            </table>
                                        </div>
                                        

                                        
                                    </div>
                                </div>
                            </div>
                            

                            

                            
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Qrcode