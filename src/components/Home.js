import React, { Component } from 'react';
import firebase from 'firebase';
import cookie from 'react-cookies';
import swal from 'sweetalert';

class Home extends Component{
    constructor(props){
        super(props);
        this.state ={
            nama    :'',
            nomor    :'',
            email :'',
            tgl:'',
            status    :'',
            
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChangeNama = this.handleChangeNama.bind(this)
        this.handleChangeEmail = this.handleChangeEmail.bind(this)
        this.handleChangeNomor = this.handleChangeNomor.bind(this)
        this.handleChangeTanggal = this.handleChangeTanggal.bind(this)
    }
    componentWillMount(){
        
        const self = this
        const getNim = cookie.load('access')
        const databaseRef = firebase.database().ref();
        const todosRef = databaseRef.child("user").child(getNim);
        todosRef.on('value', snap => {
            if (snap.exists() !== false){
                self.setState({
                    nama    : snap.val().nama,
                    kode_prodi: snap.val().jurusan,
                    nim     : cookie.load('user_id'),
                    foto    : snap.val().foto,
                    kke     : snap.val().kke,
                    email   : snap.val().email,
                    no_hp   : snap.val().no_hp,
                    pass    : snap.val().password
                })
            }else{
                console.log('Data kosong!')
            }
        })
    }

    handleSubmit(event) {
        if (this.state.nama !== undefined && this.state.nomor !== '' && this.state.email !== '' && this.state.tgl !== '') {
        cookie.save('nama', this.state.nama, {
            path: '/'
        })
        cookie.save('email', this.state.email, {
            path: '/'
        })
        cookie.save('nomor', this.state.nomor, {
            path: '/'
        })

        setTimeout(() => {
            window.location = "/qrcode";
        }, 0);

        }else {
            swal("Oops!", "Isiq angkak semuanya!", "error");
        }
        
        // const self = this
        // self.setState({ loading: true })
        // event.preventDefault()
        // const databaseRef = firebase.database().ref();
        // if (this.state.nama !== undefined && this.state.nomor !== '' && this.state.email !== '' && this.state.tgl !== '') {
        //     const todosRef = databaseRef.child("peserta").child(this.state.nomor);
        //         setTimeout(() => {
        //             todosRef.set({
        //                 Nama: this.state.nama,
        //                 Nomor: this.state.nomor,
        //                 Email: this.state.email,
        //                 Tanggal: this.state.tgl,
        //                 status: this.state.status,
        //             });
        //         }, 0);
        //         this.setState({
        //             loading: false
        //         })
        //         swal("Oops!", "Berhasil", "Succes");
            
        // } else {
        //     this.setState({
        //         loading: false
        //     })
        //     swal("Oops!", "Isiq angkak semuanya!", "error");
        // }
    }

    handleChangeNama(event) {
        this.setState({ nama: event.target.value }) 
    }
    handleChangeEmail(event) {
        this.setState({ email: event.target.value }) 
    }
    handleChangeNomor(event) {
        this.setState({ nomor: event.target.value }) 
    }
    handleChangeTanggal(event) {
        this.setState({ tgl: event.target.value }) 
    }
    
    render() {
        return(
            <div className="main">
                <div className="main-content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-8">
                                <div className="panel">
                                    <div className="panel-heading">
                                        <h1 className="panel-title"><b>Form Pendaaftaran</b></h1><br/>
                                        <div className="table-responsive">
                                            <table className="table table-hover">
                                            <div class="container">
                                            <form onSubmit={this.handleSubmit}>
                                                
                                                <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label for="first">Name</label>
                                                    <input type="text" class="form-control" placeholder="Nama" id="first" onChange={this.handleChangeNama}/>
                                                    </div>
                                                </div>
                                             

                                                
                                                </div>


                                                <div class="row">
                                                
                                            

                                                <div class="col-md-6">

                                                    <div class="form-group">
                                                    <label for="phone">Phone Number</label>
                                                    <input type="tel" class="form-control" id="phone" placeholder="phone" onChange={this.handleChangeNomor}/>
                                                    </div>
                                                </div>
                                                
                                                </div>
                                               


                                                <div class="row">
                                                <div class="col-md-6">

                                                    <div class="form-group">
                                                    <label for="email">Email address</label>
                                                    <input type="email" class="form-control" id="email" placeholder="email" onChange={this.handleChangeEmail}/>
                                                    </div>
                                                </div>
                                                
                                               

                                               
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6">

                                                    <div class="form-group">
                                                    <label for="email">Tanggal Daftar</label>
                                                    <input type="Date" class="form-control" id="tanggal" placeholder="Tanggal"onChange={this.handleChangeTanggal}/>
                                                    </div>
                                                </div>
                                                
                                               

                                               
                                                </div>
                                                

                                                <div className="row">
                                            <div className="col-md-6">
                                            <button  type="submit">submit</button>
                                            
                                            </div>
                                            
                                        </div>
                                               
                                            </form>
                                            </div>



                                            </table>
                                        </div>
                                        

                                        
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="panel">
                                    <div className="panel-heading">
                                        <h1 className="panel-title"><b>Jumlah Peserta</b></h1><br/>
                                        <div className="metric">
                                            
                                            <p>
                                                <span className="number"><b>10</b></span>
                                               
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                            

                            
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Home